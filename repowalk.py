#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import glob
import os

from pygit2 import (Repository, Tree, Diff, Patch,
                    GIT_SORT_TOPOLOGICAL,
                    GIT_SORT_REVERSE)

from conf import Config


def allRepos():
    for path in glob.glob(os.path.join(
            Config.basedir, Config.repo_path, "*", "*", ".git")):
        yield path


def repo_commit(repo, commit):
    current_commit = repo.revparse_single(commit)
    previous_commit = current_commit.parents
    committer = current_commit.committer
    author = current_commit.author
    diff_from_prev_to_now = None

    if len(previous_commit) > 0:
        diff_from_prev_to_now = repo.diff(previous_commit[0].tree,
                                          current_commit.tree)
    else:
        diff_from_prev_to_now = current_commit.tree.diff_to_tree(swap=True)
    return {
        "committer": {
            "name": committer.name,
            "email": committer.email
        },
        "message": current_commit.message,
        "time": current_commit.commit_time,
        "author": {
            "name": author.name,
            "email": author.email
        },
        "id": str(current_commit.id),
        "tree": diff_from_prev_to_now
    }


def stats_for_all_commits(repo):
    previous_commit = None
    for commit in repo.walk(repo.head.target,
                            GIT_SORT_TOPOLOGICAL | GIT_SORT_REVERSE):
        committer = commit.committer
        author = commit.author
        diff_from_prev_to_now = None

        if previous_commit is None:
            diff_from_prev_to_now = commit.tree.diff_to_tree(swap=True)
        else:
            diff_from_prev_to_now = repo.diff(
                previous_commit.tree, commit.tree
                )

        previous_commit = commit

        yield {
            "committer": {
                "name": committer.name,
                "email": committer.email
            },
            "message": commit.message,
            "time": commit.commit_time,
            "author": {
                "name": author.name,
                "email": author.email
            },
            "id": str(commit.id),
            "tree": diff_from_prev_to_now
        }


def test():
    import json
    for repo in allRepos():
        r = Repository(repo)
        stat_format = (
            "    {old} -> {new}, add:{add}, "
            "remove:{remove} blobid: {blobid}, "
            "status: {status}, data:{data}"
            )
        for stat in stats_for_all_commits(r):
            print(("  commit: {commit}").format(commit=stat["id"]))
            for patch in stat["tree"]:
                data = ""
                try:
                    data = r[patch.new_id].data
                except Exception:
                    pass
                print(
                    stat_format.format(
                        old=patch.old_file_path, new=patch.new_file_path,
                        add=patch.additions, remove=patch.deletions,
                        status=patch.status, blobid=patch.new_id,
                        data=data)
                    )
            print("")
        print("\n")

if __name__ == "__main__":
    test()
