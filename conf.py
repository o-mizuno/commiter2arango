#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Config(object):
    basedir = "../"
    blob_dest = "/tmp"
    repo_path = "repos"
