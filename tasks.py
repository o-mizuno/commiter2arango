#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os

from arango import create
from celery import Celery
from pygit2 import Repository
from conf import Config

import repowalk
import langdetect
import nodes
import shutil

manager = Celery("manager", broker="amqp://guest@localhost/miner")
db = create(db="langcommiter")
manager.conf["CELERY_ACCEPT_CONTENT"] = ['pickle', 'json', 'msgpack', 'yaml']


def log_err(r, e, c):
    if not os.path.isdir("log"):
        os.mkdir("log")
    with open((os.path.join("log", "{reponame}.log")).format(
            reponame=os.path.basename(r)), "a") as fout:
        try:
            fout.write(("Commit: {0}\n    {1}\n").format(c, str(e)))
        except Exception:
            fout.write(("Commit: {0}\n    Unknown Error\n").format(c))


@manager.task(name="scan_commit")
def scan_commit(repo_dir, commit):
    repo = Repository(repo_dir)
    reponame = os.path.basename(repo.workdir[0:-1])
    stat = repowalk.repo_commit(repo, commit)
    print(("{reponame} {commit}").format(
        reponame=reponame,
        commit=stat["id"]))

    destdir = os.path.join(Config.blob_dest, reponame, stat["id"])
    if not os.path.isdir(destdir):
        os.makedirs(destdir)

    person = nodes.Person(stat["committer"]["name"],
                          stat["committer"]["email"])

    person_node = person.save(db)
    file_stats = dict()

    for patch in stat["tree"]:
        data = ""
        try:
            data = repo[patch.new_id].data
        except Exception as e:
            log_err(repo.workdir[0:-1], e, stat["id"])
            continue
        isStateD = patch.status == "D"
        if not isStateD:
            abs_path = os.path.join(destdir,
                                    patch.new_file_path.replace("/", "-"))
            with open(abs_path, "wb") as fout:
                fout.write(data)

            file_stats[abs_path] = {
                "rpath": patch.new_file_path,
                "add": patch.additions,
                "del": patch.deletions
                }

    files = list(file_stats.keys())
    langs = langdetect.detect_files(files)

    shutil.rmtree(destdir)

    for lang in langs:
        pre_lang_node = nodes.Language(lang.language)
        lang_node = pre_lang_node.save(db)
        add_del = file_stats[lang.filename]
        try:
            nodes.learnt(db, person_node, lang_node,
                         add_del["add"], add_del["del"])
        except BaseException as e:
            log_err(repo.workdir[0:-1], e, stat["id"])
            continue
