#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import repowalk
import langdetect
import nodes
import tasks
import os
import shutil

from arango import create
from pygit2 import Repository
from conf import Config


def main():
    db = create(db="langcommiter")
    db.database.create()
    db.People.create()
    db.People.index.create(["name"], index_type="hash")
    db.People.index.create("email", index_type="hash", unique=True)
    db.Languages.create()
    db.Languages.index.create("language", index_type="hash", unique=True)
    db.Learnt.create_edges()
    db.Learnt.index.create(["add", "del"], index_type="geo")

    for r in repowalk.allRepos():
        repo = Repository(r)
        try:
            for stat in repowalk.stats_for_all_commits(repo):
                print(("({reponame}) {commit}").format(
                    reponame=os.path.basename(repo.workdir[0:-1]),
                    commit=stat["id"]))
                tasks.manager.send_task("scan_commit", [repo.path, stat["id"]])
        except Exception:
            print((
                "I got an error about this {reponame}, but ignored."
                ).format(reponame=os.path.basename(repo.workdir[0:-1])))
            continue

if __name__ == "__main__":
    main()
