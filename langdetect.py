#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import subprocess


class DetectedLanguage(object):
    __filename__ = None
    __language__ = None

    def __init__(self, fname, lang):
        self.__filename__ = fname
        self.__language__ = lang

    @property
    def filename(self):
        return self.__filename__

    @property
    def language(self):
        return self.__language__

    def __repr__(self):
        return ("(File: {file}, Language: {lang})").format(
            file=self.filename,
            lang=self.language)


def detect_files(fpaths):
    import os
    for fpath in fpaths:
        if not os.path.exists(fpath):
            raise IOError("File Not found")
    if not fpaths:
        return []

    args = ["ohcount", "-d"]
    args.extend(fpaths)
    proc = subprocess.Popen(args, stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    (out, err) = proc.communicate()
    err = err.decode("utf-8")
    if err:
        raise Exception(err)
    out.decode("utf-8")
    out = out.decode("utf-8").split("\n")
    result = list()

    for el in out:
        lang_file = el.split("\t", 1)
        if len(lang_file) == 2 and isinstance(lang_file[0], str) and\
                lang_file[0] != "(null)":
            result.append(DetectedLanguage(lang_file[1], lang_file[0]))

    return result


def __test__():
    import sys
    if len(sys.argv) < 2:
        raise ValueError("Specify filepath")

    detected_languages = detect_files(sys.argv[1:])
    print(detected_languages)

if __name__ == "__main__":
    __test__()
