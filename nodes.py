#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from arango.edge import Edge
from arango.collection import Collection

__languages__ = []


class Person(object):
    name = None
    email = None

    def __init__(self, name, email):
        self.name = name
        self.email = email

    def save(self, db):
        nperson = db.People.query.filter(
            "obj.name == @name||obj.email == @email"
            ).bind(name=self.name, email=self.email).execute().first

        if not nperson:
            nperson = db.People.documents.create({
                "name": self.name,
                "email": self.email
                })
        if nperson is None:
            raise BaseException(
                ("nperson shouldn't be None:"
                    " name={name}, email={email}").format(
                        name=self.name, email=self.email))
        return nperson


class Language(object):
    lang = None

    def __init__(self, lang):
        self.lang = lang

    @property
    def language(self):
        return self.lang

    def save(self, db):
        nlang = db.Languages.query.filter(
            "obj.language == @lang"
            ).bind(lang=self.language).execute().first

        if not nlang:
            nlang = db.Languages.documents.create({
                "language": self.lang
                })

        return nlang


def learnt(db, nperson, nlang, append_loc=0, remove_loc=0):
    corresponding_relation = None

    try:
        corresponding_relation = db.Learnt.query.filter(
            "obj._from == @nperson_id && obj._to == @nlang_id").bind(
            nperson_id=nperson.id, nlang_id=nlang.id
            ).execute().first
    except Exception as e:
        print(nperson, nlang)
        raise e

    if corresponding_relation:
        corresponding_relation.update({
            "add": corresponding_relation.get("add") + append_loc,
            "del": corresponding_relation.get("del") + append_loc,
            })
    else:
        db.Learnt.edges.create(nperson, nlang, {
            "add": append_loc,
            "del": remove_loc
            })


def __test():
    from arango import create
    db = create(db="langcommiter")
    db.database.create()
    db.People.create()
    db.People.index.create(["name"], index_type="hash")
    db.People.index.create("email", index_type="hash", unique=True)
    db.Languages.create()
    db.Languages.index.create("language", index_type="hash", unique=True)
    db.Learnt.create_edges()
    db.Learnt.index.create(["add", "del"], index_type="geo")

    hyamamoto = Person("Hiroaki Yamamoto", "admin@hysoftware.net")
    languages = [
        Language("C++"),
        Language("C++"),
        Language("C"),
        Language("Python"),
        Language("Javascript"),
        Language("HTML")
        ]
    n_hyamamoto = hyamamoto.save(db)
    for langauge in languages:
        n_lang = langauge.save(db)
        learnt(db, n_hyamamoto, n_lang, 25, 25)
        learnt(db, n_hyamamoto, n_lang, 25, 25)
        learnt(db, n_hyamamoto, n_lang, 25, 25)
        learnt(db, n_hyamamoto, n_lang, 25, 25)

if __name__ == "__main__":
    __test()
